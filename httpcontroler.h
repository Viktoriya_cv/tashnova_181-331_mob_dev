#ifndef QHTTPCONTRILLER_H
#define QHTTPCONTRILLER_H

#include <QObject>
#include<QNetworkAccessManager>

class HttpController : public QObject
{
    Q_OBJECT
public:
    explicit HttpController(QObject *parent = nullptr);
    QNetworkAccessManager *nam ; //класс http клиента - ответственен за открытие сокета/настройка сокета(  в общем настройка сокета)
    //работа с зпголовками ( с первой стартовой строкой) на ожидание по таймауту

public slots:
    void GetNetworkValue();
   // void SlotFinished(QNetworkReply *reply);
signals:
    void signalSendToQML(QString pReply);

};

#endif // HTTPCONTRILLER_H
