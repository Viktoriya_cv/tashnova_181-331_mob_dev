#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QPixmap>
#include <QQmlContext>
#include "httpcontroler.h"

//QCoreApplication ( только консолька т.е. самый базовый класс отвечает Qobj Slotsignal за обработку командной строки и т.д )
//QGuiApplication ( позволяет работать с графикой: приложение с графической областью)
//QApplication ( с виджетами)

//классы выше наследуются по нарастающей (здесь сверху вниз)

int main(int argc, char *argv[])
{
    //вызов независимой  функции в составе класса QCoreApplication
    // без создания экземпляра класса (объекта)
    // просто настройка масштабирования экрана
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv); // создаётся базовое приложение с графической областью

    HttpController httpController;

    QQmlApplicationEngine engine; // создание браузерного движка

    QQmlContext *context = engine.rootContext();
    context->setContextProperty("httpController", &httpController); //поместить С++объект в область видимости движка QML

    //httpController.GetNetworkValue();

     // преобразование пути стартовой страницы из char в QURL
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    // подключение слота, сробатывающего по сигналу objectCreated
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app,
                     [url](QObject *obj, const QUrl &objUrl) // заголовок лямбда-выражения- (функция без названия) вместо отдельного слота
    { // тело лямбда-выражения
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);


    engine.load(url); // загрузка стартовой страницы с адресом url

    QObject * mainWindow = engine.rootObjects().first();
    QObject::connect(mainWindow, SIGNAL(signalMakeRequest()),
            &httpController, SLOT(GetNetworkValue()));

    return app.exec(); //запуск бесконечного цикла обработки сообщений и слотов/сигналов
}
