import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtQuick.Controls.Universal 2.12
import QtQuick.Dialogs 1.3
import QtMultimedia 5.14
import QtWebView 1.1


ApplicationWindow {
    id: mainWindow // если к объекту не планируется обращаться, можно без id
    signal signalMakeRequest()

    visible: true
    width: 520
    height: 700
    title: qsTr("Tabs") // qsTr - функция для переключения языка строки

    Connections{
        target: httpController // объект - источник сигнала, его необходимо сделать видимым в QML
        function onSignalSendToQML(pString){
            textArea.append(pString);
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page { //anchors
            Button{
                id: button1
                text: "кнопка 1"
                //font.family: "Arial"
                //font.pixelSize: 30
                anchors.top: parent.top
                anchors.margins: 20

                font{
                    family: "Arial"
                    pixelSize: 30
                }

            }

            Button{

                text: "кнопка 2"
                //font.family: "Arial"
                //font.pixelSize: 30
                anchors.right: parent.right
                anchors.left: button1.right
                anchors.top: button1.bottom
                anchors.margins: 20
                height: 50

                font{
                    family: "Arial"
                    pixelSize: 30
                }

            }

            Button{

                text: "кнопка 3"
                //font.family: "Arial"
                //font.pixelSize: 30
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 20
                height: 100
                width: 100

                font{
                    family: "Arial"
                    pixelSize: 30
                }

            }
        }

        Page { //layout
            GridLayout {
                anchors.fill: parent
                columns: 3
                Button{
                    id: button4
                    text: "кнопка 1"

                    font{
                        family: "Arial"
                        pixelSize: 30
                    }

                }

                Button{
                    id: button5
                    text: "кнопка 2"

                    font{
                        family: "Arial"
                        pixelSize: 30
                    }

                }

                Button{
                    id: button6
                    text: "кнопка 3"

                    font{
                        family: "Arial"
                        pixelSize: 30
                    }

                }

                Button{
                    id: button7
                    text: "кнопка 4"

                    font{
                        family: "Arial"
                        pixelSize: 30
                    }

                }

                Button{
                    id: button8
                    text: "кнопка 5"

                    Layout.row: 2
                    Layout.column: 2
                    //Layout.fillHeight: true
                    //Layout.fillWidth: true

                    Layout.preferredHeight:200
                    Layout.preferredWidth: 200
                    font{
                        family: "Arial"
                        pixelSize: 30
                    }

                }

            }
        }

        Page { //Lab1

            Item {
                id: item1
                width: mainWindow.width
                height: 100

                LinearGradient {
                    anchors.fill: parent
                    start: Qt.point(0, 0)
                    end: Qt.point(0, 100)
                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "mediumturquoise" }
                        GradientStop { position: 1.0; color: "royalblue" }
                    }

                }
            }

            Image {
                id:img
                width: 70
                height: 50
                source: "qrc:/img/img/sky.png"
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 10
            }

            Text {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: img.right
                anchors.right: parent.right
                anchors.margins: 10

                text: qsTr("Лабораторная работа №1.<br> Основы разработки приложений Qt QML.<br> Элементы графического интерфейса")
                color: 'white'
                font{
                    family: "Tahoma"
                    pixelSize: 20
                    bold:true
                }
            }

            Text {
                id:tf2
                anchors.top:item1.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 5
                color: "royalblue"
                text: qsTr("Настройки:")
                font{
                    family: "Tahoma"
                    pixelSize: 25
                }
            }

            TextField {
                id: tf1
                color: 'royalblue'
                placeholderText: qsTr("Введите имя")
                //validator: IntValidator {bottom: 0; top: 100;}
                //focus: true
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: tf2.bottom
                anchors.margins: 10
                height: 50

                font{
                    family: "Tahoma"
                    pixelSize: 20
                }

            }

            Text {
                id:tf3
                anchors.top:tf1.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 5
                text: qsTr("Громкость:")
                color: "royalblue"
                font{
                    family: "Tahoma"
                    pixelSize: 20

                }
            }

            Slider {
                id:sl2
                anchors.top:tf3.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 10
                from: 1
                value: 38
                to: 100
            }

            GridLayout{
                id:layout1
                anchors.top: sl2.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 20
                columns:2

                ColumnLayout{

                    Text {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        color: "royalblue"
                        text: qsTr("Загрузка изоображения:")
                        font{
                            family: "Tahoma"
                            pixelSize: 20
                        }
                    }

                    BusyIndicator {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        running: image.status === Image.Loading
                    }
                }

                RowLayout{

                    ColumnLayout{

                        Text{text: qsTr("Микрофон:")
                            color: "royalblue"
                            font{
                                family: "Tahoma"
                                pixelSize: 20
                            }
                        }

                        Switch {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                        }
                    }

                    ColumnLayout{

                        Text{text: qsTr("Камера:")
                            color: "royalblue"
                            font{
                                family: "Tahoma"
                                pixelSize: 20
                            }
                        }

                        Switch {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                        }
                    }

                }
            }

            Text{
                anchors.margins: 10
                anchors.top: layout1.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                id:text
                color: "royalblue"
                text: qsTr("Сканирование:")
                font{
                    family: "Tahoma"
                    pixelSize: 20
                }
            }

            DelayButton {
                id: control1
                anchors.top: text.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 10
                checked: true
                text: qsTr("Приложите\nПалец")

                contentItem: Text {
                    text: control1.text
                    font: control1.font
                    opacity: enabled ? 1.0 : 0.3
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 100
                    opacity: enabled ? 1 : 0.3
                    color: control1.down ? "#1e90ff" : "mediumturquoise"
                    radius: size / 2

                    readonly property real size: Math.min(control1.width, control1.height)
                    width: size
                    height: size
                    anchors.centerIn: parent

                    Canvas {
                        id: canvas
                        anchors.fill: parent

                        Connections {
                            target: control1
                            onProgressChanged: canvas.requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d")
                            ctx.clearRect(0, 0, width, height)
                            ctx.strokeStyle = "white"
                            ctx.lineWidth = parent.size / 20
                            ctx.beginPath()
                            var startAngle = Math.PI / 5 * 3
                            var endAngle = startAngle + control1.progress * Math.PI / 5 * 9
                            ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth / 2 - 2, startAngle, endAngle)
                            ctx.stroke()
                        }
                    }
                }
            }

            Button{
                id: control
                anchors.top: control1.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.margins: 10

                background: Rectangle {

                    implicitWidth: 250
                    implicitHeight: 45
                    opacity: enabled ? 1 : 0.3
                    color: "royalblue"
                    radius: 15

                }
                Text{
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    color: "white"
                    text: qsTr("Сохранить изменения")
                    font{
                        family: "Tahoma"
                        pixelSize: 20
                    }
                }
            }

        }

        Page { //Lab2

            RowLayout{
                id:switch_row
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    text: camera_switch.checked ? "Камера":"Видео"
                    color: "royalblue"
                    font{
                        family: "Tahoma"
                        pixelSize: 20
                    }
                }

                Switch {
                    id:camera_switch
                }
            }

            Rectangle {
                anchors.margins: 10
                anchors.bottomMargin: 70
                anchors.top: switch_row.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                color: "#333333"

                Item{

                    visible: camera_switch.checked ? false:true
                    anchors.fill: parent

                    MediaPlayer {
                        id: mediaPlayer
                        muted: false
                        source: "qrc:/video/video/sample.webm"
                    }

                    VideoOutput {
                        id: video
                        source: mediaPlayer
                        anchors.fill: parent
                        anchors.margins: 5
                        focus: visible
                    }

                    MouseArea {
                        visible: camera_switch.checked ? false:true
                        id: area
                        anchors.fill: parent
                        opacity: 1.0
                        onClicked: {

                            timer.running = true
                            if (area.opacity == 0.0) {opacity.running = true} else {
                                no_opacity.running = true

                            }


                        }

                        Timer {
                            id: timer
                            interval: 7000
                            running: false
                            repeat: false
                            onTriggered: no_opacity.running = true
                        }

                        SequentialAnimation {
                            id: no_opacity
                            NumberAnimation {
                                target: area
                                properties: "opacity"
                                from: 1.0
                                to: 0.0
                                duration: 100
                            }

                        }

                        SequentialAnimation {
                            id: opacity
                            NumberAnimation {
                                target: area
                                properties: "opacity"
                                from: 0.0
                                to: 1.0
                                duration: 100
                            }

                        }

                        Button {
                            id: playButton
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            enabled: mediaPlayer.hasAudio

                            icon.source: mediaPlayer.playbackState === MediaPlayer.PlayingState ? "qrc:/img/img/stop.png" : "qrc:/img/img/play.png"
                            onClicked: mediaPlayer.playbackState === MediaPlayer.PlayingState ? mediaPlayer.pause() : mediaPlayer.play()
                            icon.color: "transparent"

                            background: Rectangle {
                                color: "#00000000"
                            }

                        }

                        RowLayout{
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.margins: 5

                            Button {
                                id: openButton
                                onClicked: fileDialog.open()

                                FileDialog {
                                    id: fileDialog

                                    folder : shortcuts.movies
                                    title: qsTr("Open file")
                                    nameFilters: [qsTr("MP3 files (*.mp4)"), qsTr("All files (*.*)")]
                                    onAccepted: mediaPlayer.source = fileDialog.fileUrl
                                }
                                icon.source: "qrc:/img/img/ss2.png"
                                icon.color: "transparent"
                                icon.height: 35
                                background: Rectangle {
                                    color: "#00000000"
                                }
                            }

                            Button{
                                id:volumeButton
                                icon.source: mediaPlayer.muted ? "qrc:/img/img/no_sound.png":"qrc:/img/img/sound.png"
                                icon.color: "transparent"
                                background: Rectangle {
                                    color: "#00000000"
                                }
                                onClicked: {
                                    if (!mediaPlayer.muted) {mediaPlayer.muted = true} else {mediaPlayer.muted = false}
                                }

                            }


                            Slider {
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                id: positionSlider
                                to: mediaPlayer.duration

                                property bool sync: false

                                onValueChanged: {
                                    if (!sync)
                                        mediaPlayer.seek(value)
                                }

                                Connections {
                                    target: mediaPlayer
                                    onPositionChanged: {
                                        positionSlider.sync = true
                                        positionSlider.value = mediaPlayer.position
                                        positionSlider.sync = false
                                    }
                                }
                            }

                            Label {
                                id: positionLabel

                                readonly property int minutes: Math.floor(mediaPlayer.position / 60000)
                                readonly property int seconds: Math.round((mediaPlayer.position % 60000) / 1000)

                                text: Qt.formatTime(new Date(0, 0, 0, 0, minutes, seconds), qsTr("mm:ss"))
                                color: "white"
                            }
                        }
                    }
                }

                Item {
                    anchors.fill: parent
                    visible: camera_switch.checked ? true:false

                    Camera {
                        id: camera

                        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

                        exposure {
                            exposureCompensation: -1.0
                            exposureMode: Camera.ExposurePortrait
                        }

                        flash.mode: Camera.FlashRedEyeReduction

                        imageCapture {
                            onImageCaptured: {
                            }
                        }


                    }

                    VideoOutput {
                        source: camera
                        anchors.fill: parent
                        focus : visible
                    }

                    Button{

                        anchors.bottom: parent.bottom
                        anchors.margins: 25
                        anchors.horizontalCenter: parent.horizontalCenter

                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Сделать фото")
                            color: "white"
                            font{
                                family: "Tahoma"
                                pixelSize: 20
                            }
                        }

                        background: Rectangle {

                            implicitWidth: 250
                            implicitHeight: 45
                            color: "royalblue"
                            radius: 15

                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                camera.imageCapture.captureToLocation("D:/proekt/tashnovaVA_181_331/gallery")
                            }
                        }
                    }


                }

            }
        }

        Page { //Lab3

            GridLayout{
                anchors.fill: parent
                anchors.bottomMargin: 5
                columns: 2

                Item {
                    height: 150
                    width: 150

                    Image {
                        id: img1
                        source: "qrc:/img/img/lb2.jpeg"
                        sourceSize: Qt.size(parent.width, parent.height)
                        smooth: true
                        visible: false
                    }

                    ZoomBlur {
                        anchors.fill: img1
                        source: img1
                        horizontalOffset:hoffset.value
                        verticalOffset:voffset.value
                        samples: samples.value
                        length: length.value
                        transparentBorder :border.checked
                    }
                }

                ColumnLayout{

                    Text {
                        id: name
                        text: qsTr("ZoomBlur.")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                            pixelSize: 20
                        }
                    }

                    Slider{
                        id:samples

                        to:10
                        from:0
                    }

                    Text {
                        text: qsTr("horizontal:")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                        }
                    }
                    Slider{
                        id:hoffset
                        value:0
                        to:-100
                        from:100
                    }

                    Text {
                        text: qsTr("vertical:")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                        }
                    }
                    Slider{
                        id:voffset
                        value:0
                        to:-100
                        from:100
                    }

                    RowLayout{
                        ColumnLayout{
                            Text {
                                text: qsTr("length:")
                                color: "royalblue"
                                font{
                                    family: "Tahoma"
                                }
                            }
                            Slider{
                                id:length
                                to:100
                                from:0
                            }
                        }
                        ColumnLayout{
                            Text {
                                text: qsTr("transparentBorder:")
                                color: "royalblue"
                                font{
                                    family: "Tahoma"
                                }
                            }
                            Switch {
                                id:border
                            }
                        }
                    }
                }

                Item {

                    height: 150
                    width: 150


                    Image {
                        id:  img2
                        source:  "qrc:/img/img/lb2.jpeg"
                        sourceSize:  Qt.size( parent.width,  parent.height)
                        smooth:  true
                        visible:  false
                    }

                    Image {
                        id:  mask
                        source:  "qrc:/img/img/ss2.png"
                        sourceSize:  Qt.size( parent.width,  parent.height)
                        smooth:  true
                        visible:  false
                    }

                    OpacityMask {
                        anchors.fill:  img2
                        source:  img2
                        maskSource:  mask
                        invert: invert.checked
                    }
                }

                ColumnLayout{

                    Text {
                        text: qsTr("OpacityMask.<br> Invert:")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                            pixelSize: 20
                        }

                    }
                    Switch{
                        id:invert
                    }

                }

                Item {

                    height: 150
                    width: 150

                    Rectangle {
                        id: background
                        anchors.fill: parent
                        color: "black"
                    }

                    RectangularGlow {
                        id: effect
                        anchors.fill: rect
                        glowRadius: glow_sl.value
                        spread: spread_sl.value
                        color: "white"
                        cornerRadius:  col_sl.value
                    }

                    Rectangle {
                        id: rect
                        Image {
                            source: "qrc:/img/img/lb2.jpeg"
                            anchors.fill:parent
                        }
                        anchors.centerIn: parent
                        width: Math.round(parent.width / 1.5)
                        height: Math.round(parent.height / 2)
                    }
                }

                ColumnLayout{
                    Text {
                        text: qsTr("RectangularGlow")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                            pixelSize: 20
                        }
                    }
                    Text {
                        text: qsTr("cornerRadius:")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                        }
                    }
                    Slider{
                        id:col_sl
                        value: 50
                        to: 0
                        from: 50
                    }
                    Text {
                        text: qsTr("glowRadius:")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                        }
                    }
                    Slider{
                        id:glow_sl
                        to: 50
                        from: 0
                    }

                    Text {
                        text: qsTr("spread:")
                        color: "royalblue"
                        font{
                            family: "Tahoma"
                        }
                    }
                    Slider{
                        id:spread_sl
                        to: 1
                        from: 0
                    }
                    /* Text {
                        text: qsTr("Color:")
                    }
                    TextField {
                        id: color_field
                        color: 'royalblue'
                        placeholderText: qsTr("Введите цвет:")
                    }*/

                }
            }
        }

        Page { //Lab4

            GridLayout{
                anchors.fill: parent

                TextArea {
                    id: textArea
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.column: 0
                    Layout.row: 0
                }
                Button{
                    id: btn
                    text: "зАпрос"
                    Layout.column: 0
                    Layout.row: 1
                    onClicked: {
                        signalMakeRequest();
                    }
                }
            }
        }

        Page { //lab5

            id: pglab5
            GridLayout{
                anchors.fill: parent
                columns: 3
                rows: 3
                Item {
                    id: tm1
                    Layout.column: 0
                    Layout.row: 2
                    Layout.fillWidth: true
                }

                Button{
                    Layout.column: 1
                    Layout.row: 2
                    text: "Autorization"
                    onClicked: {
                        browser.visible = true;
                        //  browser.url =
                    }
                }

                Item {
                    id: tm2
                    Layout.column: 3
                    Layout.row: 2
                    Layout.fillWidth: true
                }

            }

            WebView{
                id: browser
                visible: false
                anchors.fill: parent
                url:""
                onLoadingChanged:{
                    /*
                if (loadRequest==WebView.LoadStartedStatus){
                }
                else if (loadRequest==WebView.LoadSucceededStatus){
                    console.info("*** " + browser.url); }

                else if (loadRequest==WebView.LoadFailedStatus){*/
                    console.info("*** " + browser.url); //}


                }
            }
        }

        Page {
            ListView{//GridView TableView для переключения в плитку
                id:lw
                anchors.fill: parent
                spacing:10

                delegate: Rectangle{
                    //anchors.fill: lw
                    color:"blue"
                    height:screen.pixelDensity * 15
                    width: lw.width

                    RowLayout{
                        anchors.fill: parent
                    CheckBox{//flag
                        checked: flag
                    }

                    Label{ //Name
                    text: name
                    font.bold: true
                    font.pixelSize: 20
                    color: "white"

                    }

                    ComboBox{//group
                       model:["171-331", "181-331","156-453" ]
                       currentIndex: group
                    }
                }
                }

                model: ListModel //модель из с++ на основе QabstractListModel
                {


                    ListElement{
                        flag: true
                        name:"Иван"
                        group:0
                    }

                    ListElement{
                        flag: true
                        name:"Alex"
                        group:1
                    }

                    ListElement{
                        flag: false
                        name:"Max"
                        group:2
                    }
                }
            }


        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Lesson1")
        }
        TabButton {
            text: qsTr("Lesson2")
        }
        TabButton {
            text: qsTr("Lab1")
        }
        TabButton {
            text: qsTr("Lab2")
        }
        TabButton {
            text: qsTr("Lab3")
        }
        TabButton {
            text: qsTr("Lab4")
        }
        TabButton {
            text: qsTr("Lab5")
        }
        TabButton {
            text: qsTr("Lab6")
        }

    }
}
